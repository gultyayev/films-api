'use strict';

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function (db) {
  return db.runSql(`DO
  $ex$
  BEGIN
  IF (SELECT NOT EXISTS (SELECT * FROM pg_extension WHERE extname = 'pgcrypto')) THEN
      CREATE EXTENSION pgcrypto;
      END IF;
  END
  $ex$`);
};

exports.down = function (db) {
  return db.runSql('DROP EXTENSION pgcrypto');
};

exports._meta = {
  "version": 1
};