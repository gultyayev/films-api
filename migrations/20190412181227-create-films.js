'use strict';

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function (db) {
  return db.createTable('films', {
    id: {
      type: 'int',
      unsigned: true,
      notNull: true,
      primaryKey: true,
      autoIncrement: true
    },
    title: {
      type: 'text',
      notNull: true
    },
    descr: {
      type: 'text'
    },
    // User that added the film
    user_id: {
      type: 'int',
      unsigned: true,
      notNull: true,
      foreignKey: {
        name: 'film_user',
        table: 'users',
        mapping: 'id',
        rules: {}
      }
    },
    // Film's producer
    producer_id: {
      type: 'int',
      unsigned: true,
      notNull: true,
      foreignKey: {
        name: 'film_producer',
        table: 'producers',
        mapping: 'id',
        rules: {}
      }
    }
  });
};

exports.down = function (db) {
  return db.dropTable('films');
};

exports._meta = {
  "version": 1
};