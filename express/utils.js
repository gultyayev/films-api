const pgp = require('pg-promise')();

module.exports.sql = function (filePath) {
    return new pgp.QueryFile(filePath, {minify: true, compress: true})
};

module.exports.errHandler = function (err) {
    if (err instanceof pgp.errors.QueryFileError) {
        console.log('QueryFile Error', err);
    } else {
        throw err;
    }
}
