const utils = require('../utils');
const db = require('../db').db;
const path = require('path');

const authUser = utils.sql(path.join(__dirname, '../queries/auth/authorize.pgsql'));

module.exports.authorize = async function (email, password) {
    try {
        const userData = await db.oneOrNone(authUser, {
            email: email,
            password: password
        });

        return userData;
    } catch (error) {
        utils.errHandler(error);
    }
}