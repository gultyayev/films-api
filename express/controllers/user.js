const utils = require('../utils');
const db = require('../db').db;
const path = require('path');

const insertUser = utils.sql(path.join(__dirname, '../queries/user/insert-user.pgsql'));

module.exports.create = async function ({
    username,
    email,
    password
}) {
    try {
        return await db.one(insertUser, {
            username: username,
            email: email,
            password: password
        });
    } catch (err) {
        utils.errHandler(error);
    };
}
