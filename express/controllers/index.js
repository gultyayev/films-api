const userController = require('./user');
const authController = require('./auth');

module.exports.User = userController;
module.exports.Auth = authController;