const express = require('express');
const app = express();
const port = 4000;
const bodyParser = require('body-parser');
const db = require('./db').db;
const Controllers = require('./controllers');
const path = require('path');

app.use(bodyParser.json());

app.use((err, req, res, next) => {
    console.error(err);
    res.status(500).json({data: 'Internal server error.'});
});

app.use(express.static(path.join(__dirname, '../angular-build')));

app.get('/', async (req, res) => {
    res.sendFile(path.join(__dirname, '../angular-build/index.html'));
});

app.post('/user', async (req, res) => {
    try {
        const request = {username, email, password} = req.body;

        const userId = await Controllers.User.create(request);

        const respObj = {
            status: 200,
            data: {
                userId: userId
            }
        };

        res.status(200).json(respObj);
    } catch (err) {
        console.error(err);
        res.sendStatus(500);
    }
});

app.post('/auth', async (req, res) => {
    try {
        const {email, password} = req.body;

        const user = await Controllers.Auth.authorize(email, password);

        if (user) {
            res.status(200).json(user);
        } else {
            res.sendStatus(404);
        }
    } catch (err) {
        console.error(err);
        res.sendStatus(500);
    }
});

app.listen(port, () => console.log(`App is running at http://localhost:${port}`));
