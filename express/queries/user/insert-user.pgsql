INSERT INTO users (email, username, password) VALUES (
    ${email},
    ${username},
    crypt(${password}, gen_salt('bf'))
) RETURNING id;