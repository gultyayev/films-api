const pgp = require('pg-promise')();

module.exports.db = pgp({
    host: 'localhost',
    port: 5432,
    database: 'films',
    user: 'postgres',
    password: '1'
});

module.exports.pgp = pgp;